import app from './src/app'
import * as Express from 'express'

const PORT = process.env.PORT || 80

app.get('/', (req: Express.Request, res: Express.Response) => {
    res.json({
        message: 'API'
    })
})

app.listen(PORT, () => {

})

