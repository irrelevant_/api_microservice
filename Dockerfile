FROM node:latest

ARG PORT

COPY . /src

WORKDIR /src

RUN npm install --production

EXPOSE ${PORT}

CMD npm start