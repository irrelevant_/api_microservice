import * as Express from 'express'
import * as bodyParser from 'body-parser'
import * as morgan from 'morgan'
import * as errorHandler from 'errorhandler'
import 'reflect-metadata'
import { v1 } from './router'
import { authMiddleware } from './middleware'
import { devError, prodError, notFoundError } from './errors'

const NODE_ENV = process.env.NODE_ENV || 'development'

const app = Express()

app.use(morgan('combined'))
app.use(bodyParser.json())
app.use(bodyParser.urlencoded({
    extended: true
}))
app.use(authMiddleware)

app.use(v1)

app.use((NODE_ENV === 'development' ? devError: prodError))
app.use(notFoundError)

app.use(errorHandler())

export default app