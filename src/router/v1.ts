import * as Express from 'express'

const API_VERSION = "v1"

let router: Express.Router
router = Express.Router()

router.get(`/${API_VERSION}/`, (req: Express.Request, res: Express.Response) => {
    // get data
    res.json({
        success: true
    })
})

export default router