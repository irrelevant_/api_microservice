import * as Express from 'express'

export const prodError = (err: any, req: Express.Request, res: Express.Response, next: Express.NextFunction) => {
    res.status(err.status || 500)
    res.json({
        error: {}, // no stack trace
        message: err.message
    })
}

export const devError = (err: any, req: Express.Request, res: Express.Response, next: Express.NextFunction) => {
    res.status(err.status || 500)
    res.json({
        error: err,
        message: err.message
    })
}

export const notFoundError = (req: Express.Request, res: Express.Response, next: Express.NextFunction) => {
    let err = new Error("Not Found")
    next(err)
}