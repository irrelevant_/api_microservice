import * as jwt from 'express-jwt'

const authMiddleware = jwt({
    secret: process.env.JWT_SECRET,
    credentialsRequired: false
})

export default authMiddleware